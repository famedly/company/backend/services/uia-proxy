/*
Copyright (C) 2020 Famedly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

import { TimedCache } from "./structures/timedcache";
import { ParamsData } from "./stages/stage";
import { SessionConfig } from "./config";
import { IPasswordProvider } from "./passwordproviders/passwordprovider";

// tslint:disable no-magic-numbers
const SESSION_ID_LENGTH = 20;
// tslint:enable no-magic-numbers

/** Data added to a session by auth stages */
export interface IExtraSessionData {
	sessionId?: string;
	/** The username this session is valid for */
	username?: string;
	/** The value to change the user's displayname to, if set */
	displayname?: string;
	/** Whether the user should be an administrator or not */
	admin?: boolean;
	/** The value to change the user's password to, if set */
	password?: string;
	passwordProvider?: IPasswordProvider;
}

// TODO: Make this a proper class so we can make more things non-nullable, the
// superfluous null checks are annoying
export interface ISessionData {
	id: string;
	params: {[type: string]: ParamsData};
	data: IExtraSessionData;
	completed?: string[];
	skippedStages: {[type: number]: Set<number>};
	endpoint: string;
}

export interface ISessionObject extends ISessionData {
	save(): void;
}

/** Object for managing the set of active login sessions. */
export class Session {
	private sessions: TimedCache<string, ISessionData>;

	constructor(
		private config: SessionConfig,
	) {
		this.sessions = new TimedCache(this.config.timeout);
	}

	/** Create a new session for the given endpoint with a randomly generated ID. */
	public new(endpoint: string): ISessionObject {
		let id = this.generateSessionId();
		while (this.sessions.has(id)) {
			id = this.generateSessionId();
		}
		const data: ISessionData = {
			id,
			params: {},
			data: {
				sessionId: id,
			},
			endpoint,
			skippedStages: {},
		};
		this.sessions.set(id, data);
		const sessionObject = data as ISessionObject;
		// Make the save method of the session object update the Session it's
		// contained within
		sessionObject.save = () => this.set(sessionObject);
		return sessionObject;
	}

	/** Get the ISessionObject with the given id */
	public get(id: string): ISessionObject | null {
		const data = this.sessions.get(id);
		if (!data) {
			return null;
		}
		const sessionObject = data as ISessionObject;
		sessionObject.save = () => this.set(sessionObject);
		return sessionObject;
	}

	/**
	 * Update the given ISessionObject. The id will be read from the object.
	 *
	 * @returns true if the session has been updated, false if no session with a matching id exists.
	 */
	public set(data: ISessionData): boolean {
		if (!this.sessions.has(data.id)) {
			return false;
		}
		this.sessions.set(data.id, data);
		return true;
	}

	private generateSessionId(): string {
		let result = "";
		const chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		const charsLen = chars.length;
		for (let i = 0; i < SESSION_ID_LENGTH; i++) {
			result += chars.charAt(Math.floor(Math.random() * charsLen));
		}
		return result;
	}
}
